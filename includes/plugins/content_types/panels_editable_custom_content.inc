<?php

/**
 * @file
 * Editable content placeholder plugin.
 */

$plugin = array(
  'title' => t('Panels editable custom content placeholder'),
  'description' => t('[TODO] Panels editable custom content placeholder'),
  'single' => TRUE,
  'no title override' => TRUE,
  'defaults' => array(
    'pecc_name' => '',
  ),
  'render callback' => 'panels_editable_custom_content_render',
  'edit form' => 'panels_editable_custom_content_edit_form',
  'admin title' => 'panels_editable_custom_content_admin_title',
  'admin info' => 'panels_editable_custom_content_admin_info',
  'category' => t('Widgets'),
);

/**
 * Render callback.
 */
function panels_editable_custom_content_render($subtype, $conf, $args, $pane_context, $incoming_content) {
  $pecc_name = $conf['pecc_name'];
  $pecc = pecc_load($pecc_name);

  $block = new stdClass();
  $block->type = 'pecc';
  $block->subtype = $pecc_name;

  if ($pecc) {
    // Hide this block if content author want so.
    if (empty($pecc->hide)) {
      $block->title = empty($pecc->title) ? NULL : check_plain($pecc->title);
      $block->content = check_markup($pecc->body, $pecc->format);
    }
  }
  else {
    $block->title = NULL;
    $block->content = '&nbsp;';

    // If user has access to edit peccs lets show him invitation to do it.
    if (user_access('edit pecc')) {
      $title = explode('_', $pecc_name);
      $title_first = reset($title);

      if (is_string($title_first)) {
        $title[0] = ucfirst($title_first);
      }

      $block->title = check_plain(implode(' ', $title));
      $content = _pecc_not_exists_message_content($pecc_name);
      $block->content = $content;
    }
  }

  if (user_access('edit pecc')) {
    // @todo For some reason selecting bootstrap panel style hides this links.
    $block->admin_links = array(
      array(
        'title' => t('Edit content'),
        'alt' => t('[TODO] Description.'),
        'href' => 'admin/content/pecc/edit/' . $pecc_name,
        'query' => drupal_get_destination(),
      ),
    );
  }

  return $block;
}

/**
 * Helper to build notification message for not existing pecc.
 *
 * @param string $pecc_name
 *   PECC machine name.
 *
 * @return string
 *   Rendered notification content.
 */
function _pecc_not_exists_message_content($pecc_name) {
  $edit_path = 'admin/content/pecc/edit/' . $pecc_name;
  $options = array('query' => drupal_get_destination());
  $vars = array(
    '!this' => l(t('this'), $edit_path, $options),
  );
  // @todo This must be improved in order to attract more user attention.
  $content = t('This content placeholder supposed to show peace of content that is not currently exists. You can fix this by clicking !this link.', $vars);

  return $content;
}

/**
 * Edit form builder.
 */
function panels_editable_custom_content_edit_form($form, &$form_state) {

  $options = array(t('None(for now)'));

  $form['pecc_name'] = array(
    '#type' => 'select',
    '#title' => t('Content that will be displayed in this pane'),
    '#description' => t('Description will go here.'),
    '#options' => $options + pecc_get_options(),
    '#default_value' => $form_state['conf']['pecc_name'],
  );

  return $form;
}

/**
 * Edit form submit handler.
 */
function panels_editable_custom_content_edit_form_submit($form, &$form_state) {
  $form_state['conf']['pecc_name'] = $form_state['values']['pecc_name'];
}

/**
 * Admin title callback.
 */
function panels_editable_custom_content_admin_title($subtype, $conf, $pane_context) {
  if (empty($conf['pecc_name'])) {
    return t('Editable custom content placeholder');
  }

  return t('Editable custom content placeholder for %name', array('%name' => $conf['pecc_name']));
}

/**
 * Admin info callback.
 */
function panels_editable_custom_content_admin_info($subtype, $conf, $context) {
  $pecc_name = $conf['pecc_name'];
  $pecc = pecc_load($pecc_name);

  $block = new stdClass();
  $block->title = t('Editable content peace info');
  if ($pecc) {
    $tpl = '<strong>!k</strong> — !v';
    $vars = array(
      'type' => 'ul',
      'items' => array(
        format_string($tpl, array('!k' => t('Machine name'), '!v' => check_plain($pecc->name))),
        format_string($tpl, array('!k' => t('Title'), '!v' => empty($pecc->title) ? drupal_placeholder(t('No title')) : check_plain($pecc->title))),
        format_string($tpl, array('!k' => t('Hide'), '!v' => empty($pecc->hide) ? t('No') : t('Yes'))),
      ),
    );
    $block->content = theme('item_list', $vars);
  }
  else {
    $block->content = _pecc_not_exists_message_content($pecc_name);
  }

  return $block;
}
