<?php

/**
 * @file
 * Administration screen callbacks.
 */

/**
 * PECC admin list page callback.
 */
function pecc_list() {
  if (module_exists('views')) {
    // @todo Add ability to swap hardcoded table with a view.
  }

  $vars = array(
    'header' => array(
      t('Name'),
      t('Title'),
      t('Hide'),
      t('Format'),
      t('Body'),
      array(
        'data' => t('Operations'),
        'colspan' => 2,
      ),
    ),
    'rows' => array(),
    'empty' => t('There is no editable content peaces.'),
  );

  $peccs = pecc_load_all();
  $formats = filter_formats();

  foreach ($peccs as $pecc) {
    $vars['rows'][] = array(
      check_plain($pecc->name),
      empty($pecc->title) ? drupal_placeholder(t('No title')) : check_plain($pecc->title),
      empty($pecc->hide) ? t('No') : t('Yes'),
      isset($formats[$pecc->format]) ? check_plain($formats[$pecc->format]->name) : t('Unknown'),
      check_markup($pecc->body, $pecc->format),
      l(t('Edit'), 'admin/content/pecc/edit/' . $pecc->name),
      l(t('Delete'), 'admin/content/pecc/delete/' . $pecc->name),
    );
  }

  return theme('table', $vars);
}

/**
 * PECC edit form builder.
 */
function pecc_edit_form($form, &$form_state, $pecc_name = NULL) {
  // Attempt to edit existing content or create a content with
  // specified machine name.
  if (isset($pecc_name)) {
    $pecc = pecc_load($pecc_name);
    // Creating new content with specified name.
    if (!$pecc) {
      $pecc = pecc_create();
      $pecc->name = $pecc_name;
    }
  }
  else {
    // Creating new one.
    $pecc = pecc_create();
  }

  $form_state['storage']['pecc'] = $pecc;


  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('While not required, content title is used to generate machine name. So if you want to create a peace of content without a title type couple characters here first, this will make machine name field visible, set machine name to something you see fit and then make this field empty.'),
    '#default_value' => $pecc->title,
  );

  $form['name'] = array(
    '#type' => 'machine_name',
    '#title' => t('Machine readable name'),
    '#description' => t('[TODO] Description.'),
    '#default_value' => $pecc->name,
    '#maxlength' => 255,
    '#disabled' => !empty($pecc->cid) || !empty($pecc->name),
    '#machine_name' => array(
      'exists' => 'pecc_load',
      'source' => array('title'),
    ),
  );

  $form['body'] = array(
    '#type' => 'text_format',
    '#title' => t('Content body'),
    '#description' => t('[TODO] Description.'),
    '#default_value' => $pecc->body,
    '#format' => empty($pecc->format) ? NULL : $pecc->format,
    '#required' => TRUE,
  );

  $form['hide'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide'),
    '#description' => t('Checking this will hide content placeholder that is supposed to show this content.'),
    '#default_value' => $pecc->hide,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  if (!empty($pecc->cid) && user_access('delete pecc')) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('pecc_edit_form_delete_submit'),
    );
  }

  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/content/pecc'),
  );

  return $form;
}

/**
 * PECC edit form submit handler.
 */
function pecc_edit_form_submit($form, &$form_state) {
  $pecc = $form_state['storage']['pecc'];

  $pecc->name = $form_state['values']['name'];
  $pecc->title = $form_state['values']['title'];
  $pecc->body = $form_state['values']['body']['value'];
  $pecc->format = $form_state['values']['body']['format'];
  $pecc->hide = $form_state['values']['hide'];

  pecc_save($pecc);

  drupal_goto('admin/content/pecc');
}

/**
 * Edit form delete button submit handler.
 *
 * Redirects uder to delete confirmation form.
 */
function pecc_edit_form_delete_submit($form, &$form_state) {
  $destination = array();
  if (isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
  }
  $form_state['redirect'] = array('admin/content/pecc/delete/' . $form_state['storage']['pecc']->name, array('query' => $destination));
}

/**
 * PECC delete confirmation form builder.
 */
function pecc_delete_form($form, &$form_state, $pecc) {
  $form_state['storage']['pecc'] = $pecc;

  return confirm_form(
    $form,
    t('Are you sure you want to delete %title peace of content?', array('%title' => empty($pecc->title) ? t('No title(@machine_name)', array('@machine_name' => $pecc->name)) : $pecc->title)),
    'admin/content/pecc'
  );
}

/**
 * PECC delete submit handler.
 */
function pecc_delete_form_submit($form, &$form_state) {
  pecc_delete($form_state['storage']['pecc']->name);
  drupal_goto('admin/content/pecc');
}
